public class AccountListController {
    
    @AuraEnabled
    public static List < Account > fetchAccts(String AccSearch)
    {
           System.debug('***'+AccSearch);
        String AccountSearch = '%'+AccSearch+'%';

        List<Account> lstAcc=[ SELECT Id, Name, Industry, Type FROM Account where name like :AccountSearch LIMIT 100 ];
             system.debug('+++++'+lstAcc);
            return lstAcc;
    }
       @AuraEnabled
    public static List<opportunity> fetchOppts(List<id> fetchOppId)
    {
        System.debug('$$$$$'+fetchOppId);
        List<Opportunity> oppList= [Select id,Name,StageName,Type,Closedate from opportunity where AccountId IN:fetchOppId];
        System.debug('$$$$$'+oppList);
        return oppList;
    }
    
    }