@istest
public class AccountListControllerTest {
    private static testMethod void unittest1(){
    Account acc = new Account();
    acc.Name = 'Test Account1';
    acc.Phone = '2444429';
    acc.Type = 'Prospect';
	acc.Industry = 'Consulting';
    
    Opportunity opp = new Opportunity();
    opp.Name = 'Test Opportunity1';
    opp.CloseDate = Date.newInstance(2019, 4, 30);
    opp.StageName = 'Prospecting';
    opp.AccountId = acc.Id;
        
    /*String AccSearch = 'Test Account1';
    List<Account> acclist = new List<Account>();
    acclist.add(acc);
    */       
    
    string accsearch = 'Test Account1';
    List <Id> oppidList = new List<Id>();
    oppIdList.add(opp.Id);
        
    Test.startTest();
    List <Account> accList = AccountListController.fetchAccts(accsearch);
    List <Opportunity> oppLIst = AccountListController.fetchOppts(oppIdList);
    Test.stopTest();
    System.assertequals(accList.size(), 1, 'Test not passed');
    System.assertequals(oppLIst.size(), 1, 'Test not passed');
    }

}