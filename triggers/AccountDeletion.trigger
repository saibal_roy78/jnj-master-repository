trigger AccountDeletion on Account (before delete) {
   
    // Prevent the deletion of accounts if they have related opportunities.
    // Best practice: don't define the list first. Always use SOQL in the For loop if it's a straightforward query.
    for (Account a : [SELECT Id FROM Account WHERE Id IN (SELECT AccountId FROM Opportunity) AND 
                     Id IN :Trigger.old]) 
    {
        Trigger.oldMap.get(a.Id).addError('Cannot delete account with related opportunities.');
    }
    
}